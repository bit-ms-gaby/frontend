package com.muhardin.endy.training.microservices.frontend.controller;

import com.muhardin.endy.training.microservices.frontend.service.PembayaranClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CustomerController {

    @Autowired private PembayaranClientService pembayaranClientService;

    @GetMapping("/customer/list")
    public ModelMap dataCustomer() {
        return new ModelMap()
                .addAttribute("dataCustomer",
                        pembayaranClientService.daftarCustomer());
    }
}
