package com.muhardin.endy.training.microservices.frontend.service;

import com.muhardin.endy.training.microservices.frontend.dto.Customer;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Map;

@FeignClient(value = "pembayaran", fallback = PembayaranClientServiceFallback.class)
public interface PembayaranClientService {
    @GetMapping("/api/customer/")
    Iterable<Customer> daftarCustomer();
    
    @GetMapping("/api/backendinfo")
    Map<String, Object> backendInfo();
}
